package TeiProcess::Parse;

use strict;
use XML::Twig;
use Forest::LP::Parser;
use Forest::Dependency::Writer;
use IO::String;
use Capture::Tiny;
use Encode;
use IPC::Run qw/timeout kill_kill signal/;
use TeiProcess::Parse::Node;
use TeiProcess::Parse::Edge;
##use utf8;
use IPC::Open2;

sub to_utf8 {
##  return encode("utf8",$_[0]);
##  return decode("latin1",$_[0]);
  return $_[0];
}

sub XML::Twig::Elt::to_utf8 {
  my $elt = shift;
##  foreach my $att (@_) {
##    $elt->set_att( $att => to_utf8($elt->att($att)) );
##  }
  return $elt;
}

sub where_register {
  my $class = shift;
  my $cas = shift;
  my $document = $cas->document;	# the NewML document
  my $xc = $cas->{content} = [];

  foreach my $p ( $document->first_descendant(qr/HeadLine/io),
		  $document->descendants('p')) {
    if ($p->att('id')) {
      $cas->register($p);
      push(@$xc,$p);
    }
  }

}

sub process {
  my $class = shift;
  my $cas = shift;

  my $document = $cas->document;	# the NewML document
  my $root = $cas->root;
  my $xc = $cas->{content} = [];
  my $parses = $cas->add_container('parses');
  my @content = ();
  my $parser = {
		parser_handle => IO::String->new,
		out => ' ' x 10000, # preallocaate to be faster
		err => ' ' x 1000
	       };

  ##  $cas->log(3,"Parsing '$content'\n");

  my $tmp = ' ' x 20000; # preallocaate to be faster
  $tmp = '';

  my $parse_stats = 
    $cas->{parse_stats} ||= {
			     start => $cas->elapsed,
			     disamb => 0,
			     parse => 0,
			     forest => 0
			    };

  my $sid = 0;
  my $fullparse = 0;
  my $trouble = 0;

  foreach $sid (sort {$a <=> $b} keys %{$cas->{sentences}}) {
    my $dag = $cas->{sentences}{$sid}{udag};
#    $cas->log(3,"Parsing udag $dag\n\n");
    unless (exists $parser->{pid}) {
      ## sometimes, we have to restart the parser
      start_persistent_parser($cas,$parser);
    }
    $cas->log(2,"Parsing E$sid");
    my $dep;
    if (($dep = use_parser($parser,$dag,$sid,$cas))
	&& $dep->first_child('cluster')
       ) {
      $dep->cut;
      $fullparse++ if ($dep->att('mode') eq 'full');
    } else {
	$trouble++;
	$dep = build_pseudo_dep($cas,$dag,"E$sid");
      }
    $dep->paste(last_child => $parses);
    dep_register($cas,$dep);
  }

  eval {
    $parser->{pid}->finish;
  };

  if ($sid) {
    my $elapsed = $cas->elapsed;
    my $avg = sprintf("%.2fs",$elapsed / $sid);
    my $rate = sprintf("%.2f%%",100*$fullparse/$sid);
    my $parse = sprintf("%.2fs",$parse_stats->{parse} / $sid);
    my $forest = sprintf("%.2fs",$parse_stats->{forest} / $sid);
    my $xmldep = sprintf("%.2fs",$parse_stats->{xmldep} / $sid);
    my $disamb = sprintf("%.2fs",$parse_stats->{disamb} / $sid);
    $cas->log(2,"Parsing stats s=$sid avg_time=$avg ($parse/$forest/$xmldep/$disamb) success_rate=$rate trouble=$trouble");
  }
  delete $cas->{parse_stats};
  delete $cas->{sentences};
}

sub dep_register {
  my $cas = shift;
  my $dep = shift;
  $cas->register($dep);
  my @nodes = ();
  foreach my $elt ($dep->children) {
    $cas->register($elt);
    my $name = $elt->name;
    if ($name eq 'cluster') {
      $elt->to_utf8(qw/lex token/);
    } elsif ($name eq 'node') {
      $elt->to_utf8(qw/form lemma/);
      my $xnode = $cas->register_node($elt->att('id'));
      if (my $deriv = $elt->att('deriv')) {
	$cas->register_deriv($deriv,node => $elt->att('id'));
	$xnode->{deriv} = $deriv;
      }
      push(@nodes,$elt);
    } elsif ($name eq 'op') {
      $cas->register_deriv($elt->att('deriv'),
			   op => $elt->att('id'));
      $cas->register_deriv( $elt->att('deriv'),
			    span => [split(/\s+/,$elt->att('span'))]
			  );
    } elsif ($name eq 'hypertag') {
      foreach my $deriv (split(/\s+/,$elt->att('derivs'))) {
	$cas->register_deriv($deriv,
			     ht => $elt->att('id'));
      }
    } elsif ($name eq 'edge') {
      $cas->register_edge($elt);
    }
  }
  @nodes = sort {node_sort($a,$b)} @nodes;
  my $prev;
  foreach my $current (@nodes) {
    if ($prev) {
      $prev->set_att('#lnext' => $current->att('id'));
      $current->set_att('#lprev' => $prev->att('id'));
    }
    $prev = $current;
  }
}

sub build_pseudo_dep {
  my $cas = shift;
  my $dag = shift;
  my $sid = shift;
  $cas->log(1,"adding pseudo dep for $sid");
  my @dag = split("\n",$dag);
  my $ncount = 0;
  my $dep = XML::Twig::Elt->new('dependencies'
				=> { id => $sid,
				     mode => 'pseudo'
				   }
			       );
  foreach my $trans (@dag) {
    next unless ($trans =~ /^\d+/);
    my ($left,$comment,$token,$right) = ($trans =~ /^(\d+)\s+\{(.*?)\}\s+(\S+)\s+(\d+)/);
    my @lex = ();
    while ($trans =~ m{<F\s+id="(.+?)">\s*(.*?)\s*</F>}go) {
      push(@lex,"$1|$2");
    }
    $left--;
    $right--;
    $ncount++;
    my $cid = "${sid}c_${left}_${right}";
    my $nid = "${sid}n${ncount}";
    XML::Twig::Elt->new( 'cluster' =>
			 { id => $cid,
			   left => $left,
			   right => $right,
			   lex => join(' ',@lex),
			   token => $token
			 } ) -> paste(last_child => $dep);
    XML::Twig::Elt->new( 'node' =>
			 { id => $nid,
			   cluster => $cid,
			   form => $token,
			   cat => "_",
			   tree => 'lexical',
			   lemma => $token,
			 }) -> paste(last_child => $dep );
  }
  return $dep;
}

sub node_sort {
  my ($a,$b) = @_;
  my @a = split(/_/,$a->att('cluster'));
  my @b = split(/_/,$b->att('cluster'));
  return ($a[1] <=> $b[1]) || ($a[2] <=> $b[2]);
}

sub split_text {
  my $text = shift;
  ## may be necessary to break sentence on some special markers
  ## A better solution would be to transform them into EPSILON
  return split(/(=\(.+?\)=)/,$text);
}

sub start_persistent_parser {
  my $cas = shift;
  my $parser = shift;
  $parser->{in} = '';
##  $parser->{out} = "x" x 10000; # preallocaate to be faster
  $parser->{out} = '';
  $parser->{err} = '';
  my @disamb_options = split(/\s+/,$cas->config->{disamb}{opts}); 	# should use config
  $parser->{pid} = IPC::Run::start 
    ['frmg_lexer','-flush'],
      '<',\$parser->{in},
	'|',
	  ['frmg_parser','-loop','-compact','-no_occur_check','-utime','-timeout',20,'-robust','-disamb','-multi','-dstats','-depxml',@disamb_options],
	    '>pty>',\$parser->{out},
	      '2>',\$parser->{err}
		,($parser->{timeout} = timeout(30))
		|| die "couldn't run parser";
  return $parser;
}

sub use_parser {
  my $parser = shift;
  my $dag = shift;
  my $sid = shift;
  my $cas = shift;
  my $parse_stats = $cas->{parse_stats};
  $parser->{out} = '';
  $parser->{err} = '';
  $parser->{in} = $dag;
  my $output;
  $parse_stats->{parse} -= $cas->elapsed;
  eval {
    $parser->{timeout}->start(40);
##    $parser->{timeout}->start(3);
##    $parser->{pid}->pump until ($parser->{out}  =~ /^Time\s+(\d+)(.*)/mo);
    $parser->{pid}->pump until ($parser->{out}  =~ /Time\s+(\d+)(.*)/mo);
    my $time = sprintf("%.3f",$1 / 1000);
    my $stopped = $2;
    chomp $stopped;
    if ($time > 5) {
      $cas->log(3,"kill persistent parser (time=$time)");
      kill_kill $parser->{pid}, grace => 1;
      delete $parser->{pid};
    } else {
      $cas->log(4,"... E$sid parsed in ${time}s $stopped");
    }
    ##  $cas->log(4,"*** GOT FROM PARSER ".substr($output,0,200));
    ##  print STDERR "output=$output\n\n";
  };
  $output = $parser->{out};
  $parser->{in} = '';
  $parser->{out} = '';
  $parse_stats->{parse} += $cas->elapsed;
  if ($@) {    
    $cas->log(1,"*** Pb parsing E$sid: $@");
    $cas->log(1,"Restarting persistent parser");
    $cas->log(5,"OUTPUT is '$output'");
    delete $parser->{pid};
  } 
#  $cas->log(3,"parsing output is '$output'");
  if ($output) {
    my $forest = '';
    $parse_stats->{forest} -= $cas->elapsed;
    eval {
      local $SIG{'ALRM'} = sub {die "Timed out on forest convert E$sid"};
      my $timeout = 30;
      my $previous_alarm = alarm($timeout);
      ## get forest from output
      my $handle = IO::String->new($output);
      while (<$handle>) {
	/----\s+DEPXML\s+----/ and last; 
      }
      while (<$handle>) { 
	/^<depxml_time>/ and last;
	$forest .= $_;
      }
      alarm($previous_alarm);
      $handle->close;
    };
    $parse_stats->{forest} += $cas->elapsed;
    if ($@) {
      $cas->log(1,"*** Pb with forest conversion for E$sid: $@");
    }
    if ($forest) {
#      $cas->log(3,"forest is '$forest'");
      $forest = XML::Twig->new()->safe_parse($forest)->root;
    }
    return $forest;
  } else {
    $cas->log(1,"*** parsing E$sid returned no output");
    return undef;
  }
}

sub TeiProcess::CAS::register_deriv {
  my $self = shift;
  my $deriv = shift;
  my $key = shift;
  my $v = shift;
  my $derivs = $self->{derivs} ||= {};
  my $info = $derivs->{$deriv} ||= { id => $deriv };
  $info->{$key} = $v;
}

sub TeiProcess::CAS::deriv2node {
  my $self = shift;
  my $deriv = shift;
  my $info = $self->{derivs}{$deriv};
  if ($info && exists $info->{node}) {
    my $nid = $info->{node};
    return $nid, $self->id2obj($nid);
  }
}

sub TeiProcess::CAS::deriv2span {
  my $self = shift;
  my $deriv = shift;
  my $info = $self->{derivs}{$deriv};
  if ($info && exists $info->{span}) {
    return $info->{span};
  }
}

sub TeiProcess::CAS::deriv2op {
  my $self = shift;
  my $deriv = shift;
  my $info = $self->{derivs}{$deriv};
  if ($info && exists $info->{op}) {
    my $oid = $info->{op};
    return $oid, $self->id2obj($oid);
  }
}

sub TeiProcess::CAS::deriv2ht {
  my $self = shift;
  my $deriv = shift;
  my $info = $self->{derivs}{$deriv};
  if ($info && exists $info->{ht}) {
    my $hid = $info->{ht};
    return $hid, $self->id2obj($hid);
  }
}

sub TeiProcess::CAS::node2op {
  my $self = shift;
  my $node = shift;		# id or XML
  $node = $self->id2obj($node) if (ref($node) eq 'SCALAR');
  my $deriv = $node->att('deriv');
  return $deriv && $self->deriv2op($deriv);
}

sub TeiProcess::CAS::node2ht {
  my $self = shift;
  my $node = shift;		# id or XML
  $node = $self->id2obj($node) if (ref($node) eq 'SCALAR');
  my $deriv = $node->att('deriv');
  return $deriv && $self->deriv2ht($deriv);
}

sub TeiProcess::CAS::register_node {
  my $self = shift;
  my $nid = shift;		# Id
  my $nodes = $self->{nodes} ||= {};
  $nodes->{$nid} ||= 
    TeiProcess::Parse::Node->new( id => $nid,
				   xml => $self->id2obj($nid),
				   cas => $self
				 );
}

sub TeiProcess::CAS::register_edge {
  my $self = shift;
  my $edge = shift;		# XML
  my $source = $edge->att('source');
  my $target = $edge->att('target');
  my $eid = $edge->att('id');
  my $edges = $self->{edges} ||= {};
  my $xsource = $self->register_node($source);
  my $xtarget = $self->register_node($target);
  my $info = $edges->{$eid} 
    ||= TeiProcess::Parse::Edge->new( 
				      id => $eid,
				      source => $xsource,
				      target => $xtarget,
				      xml => $edge,
				      cas => $self
				     );
  ## $xtarget->{in} = $info;
  ## $xtarget->_set_in($info);
  ## $self->log(3,"target ".$xtarget->sprint." in=".$xtarget->in->id);
  ##  push(@{$xsource->{out}},$info);
  ## $xsource->add_out($info);
  ## my $extra = '';
  ## if ($xsource->in) {
  ##  $extra = " in=".$xsource->in->id;
  ## }
  ## $self->log(3,"source ".$xsource->sprint.$extra);
  ## return $info;
}

sub TeiProcess::CAS::xnode {
  my $self = shift;
  my $node = shift;		# id or XML
  my $nid = (ref($node) eq 'SCALAR') ? $node : $node->att('id');
  return $self->{nodes}{$nid};
}

sub TeiProcess::CAS::xedge {
  my $self = shift;
  my $edge = shift;		# id or XML
  my $eid = (ref($edge) eq 'SCALAR') ? $edge : $edge->att('id');
  return $self->{edges}{$eid};
}

sub TeiProcess::CAS::find_nodes {
  my $self = shift;
  my $cond = shift;		# sub {}
  my @nodes = ();
  while (my ($k,$n) = each %{$self->{nodes}}) {
    push(@nodes,$n) if ($cond->($n));
  }
  return @nodes;
}

sub TeiProcess::CAS::find_edges {
  my $self = shift;
  my $cond = shift;		# sub {}
  my @edges = ();
  while (my ($k,$e) = each %{$self->{edges}}) {
    push(@edges,$e) if ($cond->($e));
  }
  return @edges;
}

1;

__END__

=head1 NAME

NewProcess::Parse - Parse a XML-TEI document with FRMG

=head1 SYNOPSIS

  use NewProcess::Parse;

  TeiProcess::Parse::process($cas);

=head1 DESCRIPTION

NewProcess::Parse - Parse a XML-TEI document with the syntactic parser FRMG

Adds desambiguated DEP XML element <dependencies> for each sentence

=head2 EXPORT

None by default.

=head1 SEE ALSO

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2009, INRIA.

This program is free software; you can redistribute it and/or modify it under
the same terms as Perl itself.

=head1 AUTHOR

Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>

=cut
