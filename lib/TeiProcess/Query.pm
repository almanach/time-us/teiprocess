package TeiProcess::Query;

use strict;
use XML::Twig;
use UNIVERSAL qw(isa can);

sub new {
  my $this = shift;
  my $cas = shift;
  my $elt = shift || $cas->root;
  my @query = @_;
  my $class = ref($this) || $this;
  my $self = { cas => $cas,
	       res => {},
	     };
  bless $self, $class;
  return $self->process($elt,\@query) && $self->{res};
}

sub process {
  my $self = shift;
  my $elt = shift;
  my $query = shift;
  while (@$query) {
    my $type = shift @$query;
    my $desc = shift @$query;
    my @objs = ();
    if (exists $desc->{id}) {
      push(@objs,$self->{cas}->id2obj($desc->{id}));
    } else {
      push(@objs,$elt->findnodes("$type"));
    }
    @objs = grep {$self->check_desc($_,$desc)} @objs;
    return 0 unless (@objs);
    if (exists $desc->{assign}) {
      $self->{res}{$desc->{assign}} = [@objs];
    }
  }
  return 1;
}

sub check_desc {
  my $self = shift;
  my $elt = shift;
  my $desc = shift;
  my @desc = @$desc;
  while (@desc) {
    my $key = shift @desc;
    my $query = shift @desc;
    next if ($key eq 'assign');
    if ($self->can($key)) {
      return 0 unless $self->$key($elt,$query);
    } else {
      ## try as a xml attribute
      my $v = $elt->att($key);
      return 0 unless ($v);
      return 0 unless $self->check_elem($v,$query,$elt,$key);
    }
  }
  return 1;
}

sub check_elem {
  my $self = shift;
  my $v = shift;
  my $query = shift;
  my $elt = shift;
  my $key = shift;
  if (isa($query,"SCALAR")) {
    return ($v eq $query);
  } elsif (isa($query,"CODE")) {
    return $query->($v,$elt,$key,$self);
  } elsif (isa($query,"ARRAY")) {
    ## disjunction
    foreach my $subq (@$query) {
      my $t = $self->check_elem($v,$subq,$elt,$key);
      return 1 if ($t);
    }
    return 0;
  }
  return 0;
}

1;
