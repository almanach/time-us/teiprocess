package TeiProcess::Citations;

use strict;
use XML::Twig;
use Encode;
use TeiProcess::Parse::Node;
use TeiProcess::Parse::Edge;
use TeiProcess::Query::Compile;

our %exclude_verbs = ( '�tre' => 1,
		       'faire' => 1
		     );

sub process {
  my $class = shift;
  my $cas = shift;
  my $citations = $cas->add_container('citations');
  my $preds = $cas->{citations}{preds} = {};
  ## Citations attached to a citation verb
  ## could try to retrieve subject for infinitive
  my $citation_verbs = $cas->data('citations');
  my $citation_preps = $cas->data('citations')->{prep} || {};
  my $citation_nouns = $cas->data('citations')->{predom} || {};
  my $nosubj = $cas->data('citations')->{nosubj} || {};
  my $cond1 = dpath is_xcomp
    { $citation_verbs->{$_->source->lemma} || $_->target->precedes($_->source) } 
      { $_->apply(dpath source is_active) } ;
  foreach my $xcomp ($cas->find_edges($cond1)) {
    my $pred = $xcomp->source;
    ##    $pred->debug;
    my $vid = $pred->id;
    my $lemma = $pred->lemma;    
    next if ($exclude_verbs{$lemma});
    my $inv = $xcomp->target->precedes($pred) || 0;
    my $span1 = $pred->span;
    my $span2 = $xcomp->target->span;
    my $xspan2 = $xcomp->target->xspan;
    $cas->log(2,"Citation ($vid) '$lemma' inv=$inv span_pred=@$span1 span_cit=@$xspan2");
    my $citation = XML::Twig::Elt->new('citation');
    my $thema = $xcomp->target;
    $citation->set_att(pred => $lemma, 
		       nodepred => $vid,
		       '#pred' => $pred,
		       '#theme' => $thema
		      );
    $citation->set_att(mode => 'neg') if ($pred->is_neg);
    new_role('theme',
	     $thema,
	     sub {
	       my $pos = shift;
	       my $spos = $xcomp->sid2pos;
	       my $uu = $thema->xoffset;
	       $cas->log(4,"Sentence pos ".$spos->sprint." vs pos ".$pos->sprint." with inv=$inv expected=".$uu->sprint);
	       if ($inv) {
		 $pos = $pos->left_add($spos);
	       } else {
		 ## $pos = $pos->right_add($spos);
	       }
	       $pos = $pos->que_normalize->verbatim_extend;
	       $cas->log(4,"=> got ".$pos->sprint);
	       return $pos;
	     }
	    )->paste(last_child => $citation);
    if ( my $subject = ( $pred->really_try_get_subject )) {
      ##      $cas->log(2,"subject span ".$subject->sprint);
      if ($nosubj->{$subject->lemma}) {
	my $lemma = $subject->lemma;
	$cas->log(2,"impersonal subject '$lemma' => discard citation");
	next;
      }
      new_role('agent',
	       $subject,
	       \&subject_normalize
	      )->paste(last_child => $citation);
    }
    if ( my $audience = ($pred->apply(dpath all_out ( is_preparg
					target {$_->lemma eq '�'}
					all_out is_subst
					union
					is_object ) target
				     ))[0]
       ) {
      if ($inv ? $pred->precedes($audience) : $audience->precedes($pred)) {
	new_role('audience',$audience)->paste(last_child => $citation);
      }
    }
    try_find_date($pred,$citation);
    last_check_before_attach($citation,$citations);
  }

  ## Citations as quoted_sentence modifiers
  foreach my $xcomp ($cas->find_edges(dpath is_subst is_quoted_S)) {
    my $pred = $xcomp->source->in->source;
    ##    $pred->debug;
    my $vid = $pred->id;
    my $lemma = $pred->lemma;    
    next if ($exclude_verbs{$lemma});
    my $inv = $xcomp->target->precedes($pred) || 0;
    my $span1 = $pred->span;
    my $span2 = $xcomp->target->span;
    my $xspan2 = $xcomp->target->xspan;
    $cas->log(2,"Citation ($vid) '$lemma' inv=$inv span_pred=@$span1 span_cit=@$xspan2");
    my $citation = XML::Twig::Elt->new('citation');
    my $thema = $xcomp->target;
    $citation->set_att(pred => $lemma, 
		       nodepred => $vid,
		       '#pred' => $pred,
		       '#theme' => $thema
		      );
    $citation->set_att(mode => 'neg') if ($pred->is_neg);
    new_role('theme',
	     $thema,
	     sub {
	       my $pos = shift;
	       my $spos = $xcomp->sid2pos;
	       my $uu = $thema->xoffset;
	       $cas->log(4,"Sentence pos ".$spos->sprint." vs pos ".$pos->sprint." with inv=$inv expected=".$uu->sprint);
	       if ($inv) {
		 $pos = $pos->left_add($spos);
	       } else {
		 ## $pos = $pos->right_add($spos);
	       }
	       $pos = $pos->que_normalize->verbatim_extend;
	       $cas->log(4,"=> got ".$pos->sprint);
	       return $pos;
	     }
	    )->paste(last_child => $citation);
    if ( my $subject = ( $pred->really_try_get_subject )) {
      ##      $cas->log(2,"subject span ".$subject->sprint);
      if ($nosubj->{$subject->lemma}) {
	my $lemma = $subject->lemma;
	$cas->log(2,"impersonal subject '$lemma' => discard citation");
	next;
      }
      new_role('agent',
	       $subject,
	       \&subject_normalize
	      )->paste(last_child => $citation);
    }
    if ( my $audience = ($pred->apply(dpath all_out ( is_preparg
					target {$_->lemma eq '�'}
					all_out is_subst
					union
					is_object ) target
				     ))[0]
       ) {
      if ($inv ? $pred->precedes($audience) : $audience->precedes($pred)) {
	new_role('audience',$audience)->paste(last_child => $citation);
      }
    }
    try_find_date($pred,$citation);
    last_check_before_attach($citation,$citations);
  }

  ## Entity CitVerb Obj de InfSent
  $cond1 = dpath is_v  {$citation_verbs->{$_->lemma}}
    all_out is_subst is_object target {$citation_nouns->{$_->lemma}}
      all_out is_adj is_N2 target is_prep
	all_out is_subst is_S target is_v;
  foreach my $pred ($cas->find_nodes($cond1)) {
    my $vid = $pred->id;
    my $lemma = $pred->lemma;    
    next if ($exclude_verbs{$lemma});
    my $thema = ($pred->apply(dpath all_out is_subst is_object target))[0];
##    next unless $thema->apply(dpath all_out is_subst is_det target {$_->poss('+')});
    my $citation = XML::Twig::Elt->new('citation');
    $citation->set_att(pred => $lemma, 
		       nodepred => $vid,
		       '#pred' => $pred,
		       '#theme' => $thema
		      );
    $citation->set_att(mode => 'neg') if ($pred->is_neg);
    new_role('theme',
	     $thema,
	     sub {
	       my $pos = shift;
	       my $spos = $thema->sid2pos;
	       $pos = $pos->que_normalize->verbatim_extend;
	       return $pos;
	     }
	    )->paste(last_child => $citation);
    if ( my $subject = ( $pred->really_try_get_subject )
       ) {
      if ($nosubj->{$subject->lemma}) {
	my $lemma = $subject->lemma;
	$cas->log(2,"impersonal subject '$lemma' => discard citation");
	next;
      }
      new_role('agent',
	       $subject,
	       \&subject_normalize)->paste(last_child => $citation);
    }
    if ( my $audience = ($pred->apply(dpath all_out is_preparg
				        target {$_->lemma eq '�'}
				        all_out is_subst target  ))[0]
       ) {
      new_role('audience',$audience)->paste(last_child => $citation);
    }
    try_find_date($pred,$citation);
    last_check_before_attach($citation,$citations);
  }

  ## selon X, S
  my $cond2 = dpath {$_->is_pro || $_->entity || $_->indirect_entity} 
    { $_->apply(dpath in is_N2 is_subst
		source is_prep {$citation_preps->{$_->lemma}}
		in is_subst is_PP
		source is_empty
		in is_adj { $_->is_vmod || $_->is_S }
		source is_v
	       ) };
  foreach my $agent ($cas->find_nodes($cond2)) {
    my $prep = $agent->parent;
    my $empty = $prep->parent;
    next unless ($empty->apply(dpath  all_out is_adj is_incise));
    my $main = $empty->parent;
    next unless ($main);
    my $vid = $prep->id;
    my $lemma = $prep->lemma;
    $cas->log(2,"Citation ($vid) '$lemma'");
    my $citation = XML::Twig::Elt->new('citation');
    $citation->set_att( pred => $lemma, 
			nodepred => $vid,
		       '#pred' => $prep,
		       '#theme' => $main
		      );
    new_role('theme',
	     $main,
	     sub {
	       my $pos = shift;
	       return $pos->extrude($agent->in->source->better_offset);
	     }
	    )->paste(last_child => $citation);
    new_role('agent',
	     $agent
	    )->paste(last_child => $citation);
    last_check_before_attach($citation,$citations);
  }

  ## modal-like construction: the citation verb is adjoined to the citation
  my $cond3 = dpath is_v {$citation_verbs->{$_->lemma}} in is_adj is_V source is_v;
  foreach my $pred ($cas->find_nodes($cond3)) {
    my $vid = $pred->id;
    my $lemma = $pred->lemma;
    $cas->log(2,"Citation ($vid) '$lemma'");
    my $citation = XML::Twig::Elt->new('citation');
    my $parent = $pred->parent;
    $citation->set_att(pred => $lemma, nodepred => $vid,
		       '#pred' => $pred,
		       '#theme' => $parent
		      );
    $citation->set_att(mode => 'neg') if ($pred->is_neg);
    new_role('theme',
	     $parent,
	     sub {
	       my $pos = shift;
	       my $xpos = $pred->better_offset;
##	       $cas->log(5,"check ".$pos->sprint." with ".$xpos->sprint);
	       $pos = $pos->left_border($xpos)->que_normalize;
##	       $cas->log(5,"got ".$pos->sprint);
	       $pos = $pos->right_add($pred->sid2pos) 
		 unless ($parent->is_full_parse);
	       return $pos->verbatim_extend;
	     }
	    )->paste(last_child => $citation);
    if ( my $subject = ( $parent->really_try_get_subject)) {
      if ($nosubj->{$subject->lemma}) {
	my $lemma = $subject->lemma;
	$cas->log(2,"impersonal subject '$lemma' => discard citation");
	next;
      }
      new_role('agent',$subject,\&subject_normalize)->paste(last_child => $citation);
    }
    if ( my $audience = ($pred->apply(dpath all_out ( is_preparg
					target {$_->lemma eq '�'}
					all_out is_subst
					union
					is_object ) target
				     ))[0]
       ) {
      new_role('audience',$audience)->paste(last_child => $citation);
    }
    try_find_date($pred,$citation);
    last_check_before_attach($citation,$citations);
  }

  ## Citations as "verbatim", <citation-verb>+subj
  foreach my $pred ($cas->find_nodes(sub {$citation_verbs->{$_[0]->lemma}})) {
    my $vid = $pred->id;
    next if ($pred->is_full_parse);
    next if ($pred->apply(dpath all_out is_xcomp));
    my $subject = ( $pred->really_try_get_subject );
    next unless ($subject);
    my $lemma = $pred->lemma;
    next if ($exclude_verbs{$lemma});
    my $verbpos = $pred->left_nearby_verbatim;
    next unless ($verbpos);
    my $predpos = $pred->offset;
    $cas->log(2,"found nearby verbatim left of ".$pred->sprint." predpos ".$predpos->sprint." verbos ".$verbpos->sprint);
    my $xpos = $predpos->add($verbpos)->left_border($verbpos);
    my $xtxt = $xpos->txt;
    $cas->log(5,"xpos '$xtxt' ".$xpos->sprint);
    next unless ($xtxt =~ /^\s*,/);
    my $span1 = $pred->span;
    $cas->log(2,"Citation ($vid) '$lemma' span_pred=@$span1");
    my $citation = XML::Twig::Elt->new('citation');
    $citation->set_att(pred => $lemma, 
		       nodepred => $vid,
		       '#pred' => $pred,
		      );
    $citation->set_att(mode => 'neg') if ($pred->is_neg);
    my $spos = $pred->sid2pos;
    new_light_role('theme',
		   $verbpos->left_add($spos)
		  )->paste(last_child => $citation);
    new_role('agent',
	     $subject,
	     \&subject_normalize
	    )->paste(last_child => $citation);
    try_find_date($pred,$citation);
    last_check_before_attach($citation,$citations);
  }

  ## Citations as <citation_vern> : "verbatim"
  foreach my $pred ($cas->find_nodes(sub {$citation_verbs->{$_[0]->lemma}})) {
    my $vid = $pred->id;
    next if ($pred->apply(dpath all_out is_xcomp));
    my $subject = ( $pred->really_try_get_subject );
    next unless ($subject);
    my $lemma = $pred->lemma;
    next if ($exclude_verbs{$lemma});
    my $verbpos = $pred->right_nearby_verbatim;
    next unless ($verbpos);
    my $predpos = $pred->offset;
    my $xpos = $predpos->add($verbpos)->right_border($verbpos);
    my $xtxt = $xpos->txt;
    $cas->log(5,"xpos '$xtxt' ".$xpos->sprint);
    ##    next unless ($xtxt =~ /[:,]\s*$/);
    my $span1 = $pred->span;
    $cas->log(2,"Citation ($vid) '$lemma' span_pred=@$span1");
    my $citation = XML::Twig::Elt->new('citation');
    $citation->set_att(pred => $lemma, 
		       nodepred => $vid,
		       '#pred' => $pred,
		      );
    $citation->set_att(mode => 'neg') if ($pred->is_neg);
    my $spos = $pred->sid2pos;
    new_light_role('theme',
		   $verbpos->right_add($spos)
		  )->paste(last_child => $citation);
    new_role('agent',
	     $subject,
	     \&subject_normalize
	    )->paste(last_child => $citation);
    try_find_date($pred,$citation);
    last_check_before_attach($citation,$citations);
  }


##  my @children = $citations->children;
##  my %preds = (map {$_->att('nodepred') => $_} @children);
  
  ## Adding citations found by sxpipe-tmp
  foreach my $opred ($cas->find_nodes(dpath is_opening_PRED)) {
    ##    $cas->log(2,"open pred ".$opred->sprint);
    my $pred = $opred->lnext;
    next unless ($pred);
    ##    $cas->log(2,"middle pred ".$pred->sprint);
    my $cpred = $pred->lnext;
    next unless ($cpred);
    ## $cas->log(2,"close pred ".$cpred->sprint);
    next if ($preds->{$pred->id} 
	     || $preds->{$opred->id}
	     || $preds->{$cpred->id}
	    );
    my $citation = XML::Twig::Elt->new('citation');
    $citation->set_att( pred => $pred->lemma, 
			nodepred => $pred->id,
			'#pred' => $pred,
			orig => 'sxpipe'
		      );
    my $agent;
    my $thema;
    my $current = $opred;
    while (my ($t,$left,$right) = $current->left_pred_satelite) {
      if ($left && ($t eq 'AUT' || $t eq 'AUT_CL')) {
	my $ag = $left->lnext;
	$cas->log(2,"handling agent-like $t ".$ag->sprint);
	$agent = new_role('agent',
			  $ag,
			  sub {
			    my $pos = shift;
			    subject_normalize($pos->add($right->offset),$ag)
			  }
			 )->paste(last_child => $citation);
      }
      if ($left && ($t eq 'SHQ' || $t eq 'HQ')) {
	$cas->log(2,"handling thema-like $t ".$left->sprint);
	$citation->set_att('#theme' => $left);
	$thema = new_role( 'theme',
			   $left,
			   sub {
			     my $pos = shift;
			     my $spos = $left->sid2pos;
			     return $pos->add($right->offset)->left_add($spos);
			   }
			 )->paste(last_child => $citation);
	last;
      }
      $current = $left;
    }
    if (my ($t,$left,$right) = $cpred->right_pred_satelite) {
      if (!$agent && $left && ($t eq 'AUT' || $t eq 'AUT_CL')) {
	my $ag = $left->lnext;
	$cas->log(2,"handling agent-like $t ".$ag->sprint);
	$agent = new_role('agent',
			  $ag,
			  sub {
			    my $pos = shift;
			    subject_normalize($pos->add($right->offset),$ag)
			  }
			 )->paste(last_child => $citation);
      }
      if (!$thema && $left && ($t eq 'STQ' || $t eq 'TQ')) {
	$cas->log(2,"handling thema-like $t ".$left->sprint);
	$citation->set_att('#theme' => $left);
	$thema = new_role( 'theme',
			   $left,
			   sub {
			     my $pos = shift;
			     $pos->add($right->offset)
			   }
			 )->paste(last_child => $citation);
      }
    }
    next unless ($thema);
    try_find_date($pred,$citation);
    last_check_before_attach($citation,$citations);
  }

  ## Removing nested citations
  my @children = $citations->children;
  while (@children) {
    my $cit1 = shift @children;
    my $pred1 = $cit1->att('#pred');
    my $thema1 = $cit1->att('#theme');
    next unless ($thema1);
    my $sp1 = $pred1->span;
    my $xst1 = $thema1->xspan;
    my $id1 = $cit1->att('id');
    my ($sid1) = ($cit1->att('nodepred') =~ /^(E\d+)/);
    foreach my $cit2 (@children) {
      my $pred2 = $cit2->att('#pred');
      my $thema2 = $cit2->att('#theme');
      next unless ($thema2);
      my $sp2 = $pred2->span;
      my $xst2 = $thema2->xspan;
      my $id2 = $cit2->att('id');
      my ($sid2) = ($cit2->att('nodepred') =~ /^(E\d+)/);
      next unless ($sid1 eq $sid2);
      if ($sp1->[0] >= $xst2->[0] && $sp1->[1] <= $xst2->[1]) {
	$cas->log(2,"Remove citation $id1 nested in $id2");
	$cit1->cut;
      } elsif ($sp2->[0] >= $xst1->[0] && $sp2->[1] <= $xst1->[1]) {
	$cas->log(2,"Remove citation $id2 nested in $id1");
	$cit2->cut;
      }
    }
  }
}

sub last_check_before_attach {
  my $citation = shift;
  my $citations = shift;
  my $pred = $citation->att('#pred');
  my $nodepred  = $citation->att('nodepred');
  my $cas = $pred->cas;
  my $thema = $citation->first_child('theme');
  my $pred_pos = $pred->offset;
  my $start = $thema->att('start');
  my $end = $thema->att('end');
  my $agent = $citation->first_child('agent');
  if (($start <= $pred_pos->start) && ($pred_pos->end <= $end)) {
    $cas->log(1,"citation predicate ".$pred_pos->sprint." within citation thema ($start,$end) => discard");
  } elsif (0 && exists $pred->cas->{citations}{preds}{$citation->att('nodepred')}) {
    $cas->log(1,"citation predicate already used => discard");
    return;
  } elsif ($agent && 
	   ( $start <= $agent->att('start')
	       && $end >= $agent->att('end')
	   )
	  ) {
    $cas->log(1,"citation agent within citation thema ($start,$end) => discard");
    return;
  }
  my $preds = $pred->cas->{citations}{preds};
  $preds->{$nodepred} ||= [];
  foreach my $xcitation (@{$preds->{$nodepred}}) {
    my $xthema = $xcitation->first_child('theme');
    my $xstart = $xthema->att('start');
    my $xend = $xthema->att('end');
    $cas->log(1,"TEST overlapping citations ($start,$end) vs ($xstart,$xend) => discard");
    if ( (($start <= $xstart) && ($xstart <= $end))
	 || (($xstart <= $start) && ($start <= $xend))) {
      $cas->log(1,"overlapping citations ($start,$end) vs ($xstart,$xend) => discard");
      return;
    }
  }
  push(@{$preds->{$nodepred}},$citation);
  $citation->set_pos($pred_pos);
  $citation->add_id;
  $citation->paste( last_child => $citations );
}

sub new_role {
  my $role = shift;
  my $arg = shift;		# Parse::Node
  unless ($arg) {
    warn "empty arg for $role";
  }
  my $resize = shift || sub { $_[0] };
  my $x = $arg->sprint;
  ##  warn "arg step 1 $arg $x for $role\n";
  my $pos = $resize->($arg->better_offset,$arg); ## ->normalize;
##  my $pos = $arg->xoffset->normalize;
  ## warn "arg step 2 $arg for $role\n";
  my $xml = XML::Twig::Elt->new( $role
				 => { node => $arg->id },
				 $pos->txt
			       )->set_pos($pos);
  ## warn "arg step 3 $arg for $role\n";
  if (my $entity = $arg->entity) {
    $xml->set_att(entity => $entity->att('id'));
  } elsif (my $entity = $arg->indirect_entity) {
    $xml->set_att(indirect_entity => $entity->att('id'));
  }
  return $xml;
}


sub new_light_role {
  my $role = shift;
  my $pos = shift;
  my $xml = XML::Twig::Elt->new( $role,$pos->txt )->set_pos($pos);
  return $xml;
}

sub try_find_date {
  my $pred = shift;
  my $citation = shift;
  my $cas = $pred->cas;
  my $advtime = $cas->data('citations')->{advtime};
  if (my $date = ($pred->apply(dpath all_out is_adj target { $_->is_S || $_->is_VMod }
			  all_out is_subst is_time_mod target))[0]) {
    new_role('date', $date )->paste(last_child => $citation);
  } elsif (my $date = ($pred->apply(dpath all_out is_adj target is_adv
				  { $advtime->{$_->lemma} }))[0]) {
    new_role('date', $date )->paste(last_child => $citation);
  }
}

sub subject_normalize {
  my $pos = shift;		# a pos
  my $subject = shift;		# a Node
  $pos = $subject->offset if ($subject && $subject->entity);
  my $txt = $pos->txt;
  $pos->cas->log(5,"subject normalize '$txt'");
  if ($txt =~ /(['-])(il|elle|ils|on|elles|tu)(\s*)$/) {
    my $short = "$2$3";
    my $space = $3;
    return TeiProcess::Pos->new( where => $pos->where,
				  start => $pos->end - length($short),
				  end => $pos->end - length($space),
				  cas => $pos->cas
				);
  } else {
    foreach my $s ($subject->apply(dpath all_out is_adj target all_out is_subst target is_v in source)) {
      $pos->cas->log(2,"cut subject on right subordonnate ".$s->sprint);
      $pos = $pos->right_border($s->better_offset);
    }
    return $pos;
  }
}

sub TeiProcess::Pos::que_normalize {
  ## remove heading que or qu'
  my $pos = shift;
  my $txt = $pos->txt;
  if ($txt =~ s/^(que |qu')//) {
    return TeiProcess::Pos->new( cas => $pos->cas,
				  where => $pos->where,
				  start => $pos->start+length($1),
				  end => $pos->end
				);
  } else {
    return $pos;
  }
}


1;

__END__

=head1 NAME

NewProcess::Citations - Preliminary extraction of citations from the
XML Dep parts

=head1 SYNOPSIS

  use NewProcess::Citations;

  TeiProcess::Citations::process($cas);

=head1 DESCRIPTION

NewProcess::Citations - Extract citations from the DEP XML components
of some CAS.

An XML component is added, with following schema:
<citations>
  <citation author="depxml_node_id" citation="depxml_node_id"/>
  ...
</citations>

=head2 EXPORT

None by default.

=head1 SEE ALSO

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2009, INRIA.

This program is free software; you can redistribute it and/or modify it under
the same terms as Perl itself.

=head1 AUTHOR

Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>

=cut
