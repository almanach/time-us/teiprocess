package TeiProcess::Aleda::Result::Data;

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use namespace::autoclean;
extends 'DBIx::Class::Core';

__PACKAGE__->table("data");

__PACKAGE__->add_columns(
			 "key" => { data_type => "number", is_auto_increment => 1, is_nullable => 0 },
			 "name" =>  { data_type => "text" },
			 "type" =>  { data_type => "text" },
			 "weight" => { data_type => "number" },
			 "def" => { data_type => "text" },
			 "link" => { data_type => "text" },
			 "subtype" => { data_type => "text" },
			 "country_code" => { data_type => "text" },
			 "geonames_type" => { data_type => "text" },
			 "longitude" => { data_type => "text" },
			 "latitude" => { data_type => "text" },
			 "google_maps_span" => { data_type => "text" },
			);

__PACKAGE__->set_primary_key("key");

__PACKAGE__->meta->make_immutable;
1;
