package TeiProcess::Server;

use strict;
use HTTP::Server::Simple::CGI;
use base qw(HTTP::Server::Simple::CGI Net::Server);
use XML::Twig;
use TeiProcess::SxPipe;
use TeiProcess::Parse;
use TeiProcess::Align;
use TeiProcess::Melt;
use TeiProcess::Entities;
use TeiProcess::Anaphore;
use TeiProcess::Citations;
use Config::Any;
use Path::Class;
use YAML::Any qw/LoadFile DumpFile/;
use Cache::FastMmap;

use TeiProcess::Aleda;

our $server;

sub new {
  my $this = shift;
  my $self = $this->SUPER::new(@_);
  $server = $self;
  my $home = $ENV{"NEWS_PROCESS_HOME"} || __PACKAGE__->home;
  $self->{tei_process}{home} = $home;
  my $cfg = Config::Any->load_files({files => ["$home/tei_process.yml"],
				     use_ext => 1,
				    });
  my %configs = map { %$_ } @$cfg;
  foreach my $f (keys %configs) {
    print STDERR "loaded config from '$f'\n";
    $self->{tei_process}{config} ||= $configs{$f};
  }
  $self->default(xmldir => '/tmp/news');
  my $logfile = $self->default(log => "$home/var/tei_process.log");
  print STDERR "My home is $home\n";
  print STDERR "My class is $this\n";
  print STDERR "My path is $ENV{PATH}\n";
  open(LOG,">>$logfile")
    || die "can't open log file '$logfile'";
  $self->{tei_process}{log} = \*LOG;
  my $data = $self->{tei_process}{data} 
    = Cache::FastMmap->new( raw_values => 0,
			    compress => 0,
			    expire_time => 0,
			    share_file => "/tmp/tei_process.cache"
			  );
  foreach my $key (keys %{$self->config->{datafiles}}) {
    my $file = $self->config->{datafiles}{$key};
    unless ($file =~ m{^/}) {
      $file = "$home/$file";
      $self->config->{datafiles}{$key} = $file;
    }
    ## $self->log(4,"datafile $key $file");
    if (-f $file) {
      $self->log(1,"load datafile $key: $file");
      my $content = LoadFile($file) || {};
      $data->set($key => $content);
#      $content->{toto} = 1;
#      $content->{fake} = $$;
#      $data->set($key => $content);
#      foreach my $k (keys %$content) {
#	$self->log(4,"Key $key $k $content->{$k}");
#      }
    }
  }
  ## use of new Aleda entity database
  if (my $aleda = $self->config->{aleda}) {
    my $schema = TeiProcess::Aleda->connect( $aleda , "" ,"", {AutoCommit => 1});
    $self->{tei_process}{aleda} = $schema;
  }
  ## store disamb options
  if (my $disamb = $self->config->{disamb}{opts}) {
    $self->{tei_process}{disamb} = [split(/\s+/,$disamb)];
  } else {
    $self->{tei_process}{disamb} = [];
  }
  return $self;
}

sub setup_session {
  my $self = shift;
  my $data = Cache::FastMmap->new( raw_values => 0,
				   compress => 0,
				   expire_time => 0,
				   share_file => "/tmp/tei_process.cache"
				 );
  $self->{tei_process}{data} = $data;
}

sub config {
  return shift->{tei_process}{config};
}

sub data {
  my $self = shift;
  my $key = shift;
  return $self->{tei_process}{data}->get($key);
}

sub datafile {
  my $self = shift;
  my $key = shift;
  return $self->config->{datafiles}{$key};
}

sub default {
  my $self = shift;
  my $k = shift;
  my $default = shift;
  if (defined (my $x = $self->{tei_process}{config}{$k})) {
    return $x;
  } else {
    $self->{tei_process}{config}{$k} = $default;
    return $default;
  }
}

sub data_set {
  my $self = shift;
  my $key = shift;
  my $v = shift;
  $self->{tei_process}{data}->set($key,$v);
}

sub data_save {
  my $self = shift;
  my $key = shift;
  my $file = $self->datafile($key);
  my $cache = $self->{tei_process}{data};
  my $data = $cache->get($key);
  if ($data) {
    if (0) {
      foreach my $k (keys %$data) {
	$self->log(4,"data save $key $k $data->{$k}");
      }
    }
    ##    $cache->set($key => $data);
    $self->log(1,"save datafile '$key' to '$file'");
    DumpFile($file,$data);
  }
}

sub closing {
  my $self = shift;
  $self->data_save('entities');
}

sub Net::Server::pre_server_close_hook {
  my $self = shift;
  print STDERR "about to close class=".ref($self)."\n";
  $TeiProcess::Server::server->closing;
}


## From Catalyst
sub home {
    my $class = shift;

    # make an $INC{ $key } style string from the class name
    (my $file = "$class.pm") =~ s{::}{/}g;

    if ( my $inc_entry = $INC{$file} ) {
        {
            # look for an uninstalled Catalyst app

            # find the @INC entry in which $file was found
            (my $path = $inc_entry) =~ s/$file$//;
            $path ||= cwd() if !defined $path || !length $path;
            my $home = dir($path)->absolute->cleanup;

            # pop off /lib and /blib if they're there
            $home = $home->parent while $home =~ /b?lib$/;

            # only return the dir if it has a Makefile.PL or Build.PL
            if (-f $home->file("Makefile.PL") or -f $home->file("Build.PL")) {

                # clean up relative path:
                # MyApp/script/.. -> MyApp

                my $dir;
                my @dir_list = $home->dir_list();
                while (($dir = pop(@dir_list)) && $dir eq '..') {
                    $home = dir($home)->parent->parent;
                }

                return $home->stringify;
            }
        }

        {
            # look for an installed Catalyst app

            # trim the .pm off the thing ( Foo/Bar.pm -> Foo/Bar/ )
            ( my $path = $inc_entry) =~ s/\.pm$//;
            my $home = dir($path)->absolute->cleanup;

            # return if if it's a valid directory
            return $home->stringify if -d $home;
        }
    }

    # we found nothing
    return 0;
}

sub log {
  my $self = shift;
  my $level = shift;
  my $msg = shift;
  my $handle = $self->{tei_process}{log};
  print STDERR $msg,"\n";
  print $handle $msg,"\n";
}

my %dispatch = (
		'/hello' => \&resp_hello,
		'/process' => \&tei_process,
		# other url processing
	       );


sub net_server {
  return 'Net::Server::Fork';
}

sub handle_request {
  my $self = shift;
  my $cgi  = shift;
  
  my $path = $cgi->path_info();
  my $handler = $dispatch{$path};

  ##  $self->setup_session;

  if (ref($handler) eq "CODE") {
    print "HTTP/1.0 200 OK\r\n";
    $self->$handler($cgi);
    
  } elsif ($path =~ m{^/(\d{4})$}) {
    $self->ls($cgi,$1);
  } elsif ($path =~ m{^/(\d{4})/(\d{2})$}) {
    $self->ls($cgi,$1,$2);
  } elsif ($path =~ m{^/(\d{4})/(\d{2})/(\d{2})$}) {
    $self->ls($cgi,$1,$2,$3);
  } elsif ($path =~ m{^/(\d{4})/(\d{2})/(\d{2})/([A-Z0-9-]+)$}) {
    $self->get_news($cgi,$1,$2,$3,$4);
  } elsif ($path =~ m{^/(\d{4})/(\d{2})/(\d{2})/([A-Z0-9-]+)/(E\d+)$}) {
    $self->get_sentence($cgi,$1,$2,$3,$4,$5);
  } elsif ($path =~ m{^/ana/(\d{4})/(\d{2})/(\d{2})/([A-Z0-9-]+)$}) {
    $self->ana_process($cgi,$1,$2,$3,$4);
  } else {
    print "HTTP/1.0 404 Not found\r\n";
    print $cgi->header,
      $cgi->start_html('Not found'),
	$cgi->h1('Not found'),
	  $cgi->p("no action for $path\n"),
	    $cgi->end_html;
  }
}

sub ls {
  my $self = shift;
  my $cgi = shift;
  my ($year,$month,$day) = @_;
  my $xmldir = $self->{tei_process}{config}{xmldir};
  my $dir = "$xmldir/$year";
  if (defined $month) {
    $dir .= "/$month";
    if (defined $day) {
      $dir .= "/$day";
    }
  }
  print "HTTP/1.0 200 OK\r\n";
  print $cgi->header({ -type=>'text/xml',
		     });
  print <<EOF;
<?xml version="1.0" encoding="UTF-8"?>
<listing>
EOF
  foreach my $file (glob("$dir/*")){ 
    my ($base) = ($file =~ m{/([^/]+)$});
    $base =~ s/\.\S+$//o;
    print " <file>$base</file>\n";
  }
  print <<EOF;
</listing>
EOF

}

sub get_news {
  my $self = shift;
  my $cgi = shift;
  my ($year,$month,$day,$did) = @_;
  my $cas = TeiProcess::CAS->new($self);
  $cas->{did} = $did;
  $cas->{date_id} = [$year,$month,$day];
  $cas->log(1,"fetching");
  if ($cas->try_cache) {
    print "HTTP/1.0 200 OK\r\n";
    $self->news_emit($cgi,$cas);
  } else {
    print "HTTP/1.0 404 Not found\r\n";
    print $cgi->header,
      $cgi->start_html('Not found'),
	$cgi->h1('Not found'),
	  $cgi->end_html;
  }
}

sub get_sentence {
  my $self = shift;
  my $cgi = shift;
  my ($year,$month,$day,$did,$sid) = @_;
  my $cas = TeiProcess::CAS->new($self);
  $cas->{did} = $did;
  $cas->{date_id} = [$year,$month,$day];
  $cas->log(1,"fetching");
  my $xml = $cas->xml;
  if ($cas->try_cache
      && (my $s = $cas->root->first_descendant("dependencies[\@id='$sid']"))
     ) {
    my $format = $cgi->param('format');
    $format = undef unless (grep {$format eq $_} qw{gif png pdf});
    print "HTTP/1.0 200 OK\r\n";
    if (!$format) {
      print $cgi->header({ -type=>'text/xml',
			 });
      print <<EOF;
<?xml version="1.0" encoding="UTF-8"?>
EOF
      $s->print;
    } else {
      print $cgi->header(-type => "image/$format");
      open(IMG,"|forest_convert.pl -f xmldep -t dep | dot -T$format -Gcharset=latin1");
      print IMG <<EOF;
<?xml version="1.0" encoding="UTF-8"?>
EOF
      $s->print(\*IMG);
      close(IMG);
    }
  } else {
    print "HTTP/1.0 404 Not found\r\n";
    print $cgi->header,
      $cgi->start_html('Not found'),
	$cgi->h1('Not found'),
	  $cgi->end_html;
  }
}

sub resp_hello {
  my $self = shift;
  my $cgi  = shift;   # CGI.pm object
  return if !ref $cgi;
  
  my $who = $cgi->param('name');
  
  print $cgi->header,
    $cgi->start_html("Hello"),
      $cgi->h1("Hello $who!"),
	$cgi->end_html;
}


sub ana_process {
  ## process anaphore resolution using cache
  my $self = shift;
  my $cgi = shift;
  my ($year,$month,$day,$did,$sid) = @_;
  my $cas = TeiProcess::CAS->new($self);
  $cas->{did} = $did;
  $cas->{date_id} = [$year,$month,$day];
  $cas->log(1,"fetching");
  my $xml = $cas->xml;
  if ($cas->try_cache) {
    my $citations = $xml->root->first_child('citations');
    $citations->cut if ($citations);
    $cas->process(qw/Anaphore/);
    if ($citations && !$xml->root->first_child('citations')) {
      $citations->paste(last_child => $xml->root);
    }
    $cas->save;
    print "HTTP/1.0 200 OK\r\n";
    $self->news_emit($cgi,$cas);
  } else {
    print "HTTP/1.0 404 Not found\r\n";
    print $cgi->header,
      $cgi->start_html('Not found'),
	$cgi->h1('Not found'),
	  $cgi->end_html;
  }
}

sub entity_process {
  ## start from entities
  my $self = shift;
  my $cgi = shift;
  my ($year,$month,$day,$did,$sid) = @_;
  my $cas = TeiProcess::CAS->new($self);
  $cas->{did} = $did;
  $cas->{date_id} = [$year,$month,$day];
  $cas->log(1,"fetching");
  my $xml = $cas->xml;
  if ($cas->try_cache) {
    if (my $entities = $xml->root->first_child('entities')) {
      $entities->cut;
    }
    if (my $citations = $xml->root->first_child('citations')) {
      $citations->cut;
    }
    $cas->process(qw/Entities Citations Anaphore/);
    $cas->save;
    print "HTTP/1.0 200 OK\r\n";
    $self->news_emit($cgi,$cas);
  } else {
    print "HTTP/1.0 404 Not found\r\n";
    print $cgi->header,
      $cgi->start_html('Not found'),
	$cgi->h1('Not found'),
	  $cgi->end_html;
  }
}

sub tei_process {
  my $self = shift;
  my $cgi  = shift;   # CGI.pm object
  return if !ref $cgi;

  print STDERR "Log info log_level=$self->{server}{log_level}\n";

  ## my $f = $cgi->param('news');
  ## my $charset = $cgi->uploadInfo($f)->{'Content-Charset'};
  ## print STDERR "charset='$charset'\n";
  my $fh = $cgi->upload('news');

  my $cas = TeiProcess::CAS->new($self,$fh);

  if ($cgi->param('nocache') || !$cas->try_cache) {
    my @components = @{$self->config->{components}};
    $cas->process(@components);
    $cas->save;
  } 

  my $elapsed = $cas->elapsed;
  $cas->log(1,"Done processing $cas->{did} (${elapsed}s)");

  $self->news_emit($cgi,$cas);

}

sub news_emit {
  my $self = shift;
  my $cgi = shift;
  my $cas = shift;
  my $nodep = $cgi->param('nodep');
  
  if ($nodep) {
    foreach my $dep ($cas->root->children('dependencies')) {
      $dep->cut;
    }
  }

  if ($cgi->param('noout')) {
    print $cgi->header({ -type => 'text/plain'}),
      "ok $cas->{did}\n";
  } else {
    my $encoding = $cgi->http( 'HTTP_ACCEPT_ENCODING');
    print $cgi->header({ -type=>'text/xml',
		       });
    $cas->print;
  }
}

sub aleda {
  my $self = shift;
  my $name = shift;
  my $aleda = $self->{tei_process}{aleda};
#  $self->log(1,"sending0 aleda $name");
  if ($aleda && $name =~ /^(\d+):/) {
    my $key = $1;
#    $self->log(1,"sending1 aleda $key");
    my $data = $aleda->resultset('Data')->find($key);
#    $data and $self->log(1,"got '$data'");
    $data and return $data->def;
  }
}

package TeiProcess::CAS;
use XML::Twig;
use POSIX qw(strftime :sys_wait_h);
use Time::HiRes qw(gettimeofday tv_interval);
## use IO::Compress::Gzip qw/gzip $GzipError/;

## use constant XMLDIR => '/tmp/news';

sub new {
  my $this = shift;
  my $server = shift;
  my $fh = shift;
  my $class = ref($this) || $this;
  my $self = { server => $server,
	       start_time => [gettimeofday],
	     };
  bless $self, $class;
  $self->{did} = '';
  my $xml = $self->{xml} 
    = XML::Twig->new(
		     pretty_print => 'indented',
		     twig_handlers => {
				       'NewsItemId' => sub {
					 my $did = $_->trimmed_text;
					 $self->{did} = $did;
					 $self->log(1,"Start processing");
				       },
				       'DateId' => sub {
					 $self->set_date($_);
				       },
				       
				       ## for NewsML 2
				       'newsItem[@guid]' => sub {
					 my $elt = $_;
					 my $guid = $elt->att('guid');
					 my @x = split(/:/,$guid);
					 $self->{did} = $x[4];
					 $self->set_date_aux($x[3]);
					 $self->log(1,"Start processing");
				       }
				       
				      },
		    );
  
  my $newroot = XML::Twig::Elt->new( 'Aggregate'
				     => {creationTime => strftime("%F %H:%M:%S", localtime)
					}
				   );
  
  if ($fh) {
    eval {
      $xml->parse($fh);
    };
    if ($@) {
      $self->log(1,"Pb Parsing XML: $@");
    }
    my $root = $xml->root;
    # keep newsml for nomos
    my $newsml = $self->{newsml} = $root->copy();
    $root->cut;
    $root->paste( first_child => $newroot );
  }
  $xml->set_root($newroot);
  $self->{ids} = {};

  return $self;
}

sub set_date {
  my $self = shift;
  my $elt = shift;
  my $date = $elt->trimmed_text;
  my ($year,$month,$day) = ($date =~ /^(\d{4})(\d{2})(\d{2})(T.*)?$/o);
  $self->{date_id} = [$year,$month,$day];
}

sub set_date_aux {
  my $self = shift;
  my $date = shift;
  my ($year,$month,$day) = ($date =~ /^(\d{4})(\d{2})(\d{2})$/o);
  $self->{date_id} = [$year,$month,$day];
}


sub xmldir {
  shift->{server}{tei_process}{config}{xmldir};
}

sub home {
  shift->{server}{tei_process}{home};
}

sub cache_file {
  my $self = shift;
  my $did = $self->{did};
  my ($year,$month,$day) = @{$self->{date_id}};
  return $self->xmldir."/$year/$month/$day/$did.xml.gz";
}

sub log {
    my $self  = shift;
    my $level = shift;
    my $msg   = shift;
    my $date  = strftime "[%F %H:%M:%S]", localtime;
    my $did = $self->{did};
##    $self->{server}->log($level,"$date: $msg");
##    print STDERR "$date $did: $msg\n";
    $self->{server}->log($level,"$date $did: $msg");
}

sub config {
  $_[0]->{server}->config;
}

sub data {
  shift->{server}->data(@_);
}

sub save {
  my $self = shift;
  if ( my $did = $self->{did} ) {
    my $date_id = $self->{date_id};
    my ($year,$month,$day) = @$date_id;
    my $xmldir = $self->xmldir;
    mkdir "$xmldir/$year";
    mkdir "$xmldir/$year/$month";
    mkdir "$xmldir/$year/$month/$day";
    my $file = "$xmldir/$year/$month/$day/$did.xml";
    $self->log(1,"Saving CAS $did to $file");
    ## $self->{xml}->print_to_file("$file");
    ##    unlink("$file.gz");
##    my $z = new IO::Compress::Gzip "$file.gz";
    my $xml = $self->{xml};
    $xml->set_output_encoding('UTF-8');
    $xml->set_encoding('UTF-8');
    open(SAVE,"| gzip > $file.gz");
    $xml->print(\*SAVE);
#    $z->close;
    close(SAVE);
  }
}

sub has_cache {
  return (-f shift->cache_file);
}

sub try_cache {
  my $self = shift;
  my $file = $self->cache_file;
  return unless (-f $file);
  if (my $root = $self->{cache}) {    
    $root->cut;
    $root->set_att(creationTime => strftime("%F %H:%M:%S", localtime));
    $self->{xml}->set_root($root);
    return 1;
  } elsif (open(CACHE,"gunzip -c $file|")) {
    $self->log(1,"Using cache file $file");
    my $root = XML::Twig->new()->parse(\*CACHE)->root;
    $root->cut;
    $root->set_att(creationTime => strftime("%F %H:%M:%S", localtime));
    $self->{xml}->set_root($root);
    $self->{cache} = $root;
    close(CACHE);
    return 1;
  } else {
    warn "can't open cache file $file";
    return;
  }
}

sub xml {
  shift->{xml};
}

sub document {
  shift->{xml}->root->first_child(qr/NewsML|newsItem/);
}

sub newsml {
  shift->{newsml}->root;
}

sub flush {
  shift->{xml}->flush(@_);
}

sub print {
  my $xml = shift->xml;
  $xml->set_output_encoding('UTF-8');
  $xml->set_encoding('UTF-8');
  $xml->print(@_);
}

sub root {
  shift->{xml}->root;
}

sub process {
  my $self = shift;
  my $did = $self->{did};
  foreach my $module (@_) {
    my $lmod = lc($module);
    $self->{xml}->set_id_seed("${lmod}_");
    eval {
      my $class = "TeiProcess\::$module";
      my $elapsed = $self->elapsed;
      $self->log(1,"Start phase '$module' (${elapsed}s)");
      $class->process($self);
    };
    if ($@) {
      $self->log(1,"*** Pb phase '$module': $@");
    }
  }
}

sub elapsed {
  my $self = shift;
  return tv_interval($self->{start_time});
}

sub register {
  my $self = shift;
  my $o = shift;
  $o->add_id;
  $self->{ids}{$o->att('id')} = $o;
}

sub id2obj {
  my $self = shift;
  my $id = shift;
  return $self->{ids}{$id};
}

sub add_container {
  my $self = shift;
  my $kind = shift;
  my $container = XML::Twig::Elt->new($kind);
  $container->paste('last_child' => $self->root);
  return $container;
}

1;
