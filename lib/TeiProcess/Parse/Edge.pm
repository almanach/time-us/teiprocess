package TeiProcess::Parse::Edge;
use Moose;

extends 'TeiProcess::Parse::Object';

has 'source' => ( is => 'ro',
		  isa => 'TeiProcess::Parse::Node'
		);

has 'target' => ( is => 'rw',
		  isa => 'TeiProcess::Parse::Node'
		);

foreach my $x (qw{label type}) {
  has $x => ( is => 'ro',
	      isa => 'Str',
	      lazy_build => 1
	    );
  { no strict 'refs';
    *{__PACKAGE__."::_build_$x"} = sub {
      shift->xml->att($x);
    }
  }
}

sub BUILD {
  my ( $self, $params ) = @_;
  $self->target->in($self);
  $self->source->add_out($self);
}

sub sprint {
  my $self = shift;
  my $source = $self->source->sprint;
  my $target = $self->target->sprint;
  my $label = $self->label;
  my $type = $self->type;
  my $id = $self->id;
  return "$source --[$id/$label/$type]--> $target";
}

foreach my $type (qw/subst adj lexical skip/) {
  no strict 'refs';
  *{__PACKAGE__."::is_$type"} = sub {
    my $self = shift;
    my $ltype = $self->type;
    return $ltype && ($type eq $ltype);
  }
}

foreach my $label (qw/subject
		      object
		      xcomp
		      S
		      arg
		      preparg
		      scomp
		      comp
		      Infl
		      V
		      V1
		      v
		      vmod
		      Modifier
		      N2
		      N2app
		      Nc2
		      prep
		      csu
		      prel
		      pri
		      pro
		      Root
		      Punct
		      S2
		      SRel
		      SubS
		      varg
		      start
		      wh
		      starter
		      advneg
		      aux
		      ce
		      number
		      ncpred
		      CS
		      PP
		      void
		      coo
		      coo2
		      coord2
		      coord3
		      coord
		      np
		      incise
		      det
		      time_mod
		      quoted_S
		     /) {
  no strict 'refs';
  *{__PACKAGE__."::is_$label"} = sub {
    my $self = shift;
    my $llabel = $self->label;
    return $llabel && ($label eq $llabel);
  }
}


no Moose;

__PACKAGE__->meta->make_immutable;

1;
