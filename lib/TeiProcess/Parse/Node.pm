package TeiProcess::Parse::Node;
use Moose;
use MooseX::AttributeHelpers;
use TeiProcess::Query::Compile;

extends 'TeiProcess::Parse::Object';

has 'in' => ( is => 'rw', 
	      isa => 'Maybe[TeiProcess::Parse::Edge]',
	      weak_ref => 1,
	    );

has 'out' => (
	      metaclass => 'Collection::Array',
	      is => 'rw', 
	      isa => 'ArrayRef[TeiProcess::Parse::Edge]',
	      default => sub { [] },
	      provides => {
			   push => 'add_out',
			   'count' => 'out_degree',
			   'map' => 'out_map',
			   'grep' => 'out_grep',
			   'elements' => 'all_out',
			   'empty' => 'is_leaf'
			  }
	     );

foreach my $x (qw{lemma cat xcat form tree cluster}) {
  has $x => ( is => 'ro',
	      isa => 'Maybe[Str]',
	      lazy_build => 1
	    );
  { no strict 'refs';
    *{__PACKAGE__."::_build_$x"} = sub {
      shift->xml->att($x);
    }
  }
}

foreach my $x (qw{lprev lnext}) {
  has $x => ( is => 'ro',
	      isa => 'Maybe[TeiProcess::Parse::Node]',
	      lazy_build => 1
	    );
  { no strict 'refs';
    *{__PACKAGE__."::_build_$x"} = sub {
      my $self = shift;
      ## $self->cas->log(5,"try go $x ".$self->sprint);
      if (my $k = $self->xml->att('#'.$x)) {
	##	$self->cas->log(2,"going $x => $k");
	return $self->cas->{nodes}{$k};
      } else {
	return undef;
      }
    };
    my $m = "all_$x";
    *{__PACKAGE__."::all_$x"} = sub {
      my $self = shift;
      my $cond = shift || sub { 1 };
      if (my $o = $self->$x) {
	my @res = ();
	push(@res,$o) if ($cond->($o));
	push(@res,$o->$m->($cond));
	return @res;
      } else { 
	return;
      }
    };
  }
}

sub is_root {
  !(defined shift->in);
}

sub sprint {
  my $self = shift;
  my $id = $self->id;
  my $cat = $self->cat;
  my $form = $self->form;
  my $lemma = $self->lemma;
  return "$id/$cat/$lemma/$form";
}

sub parent {
  my $self = shift;
  my $cond = shift || {};
  my $edge_cond = $cond->{edge} || sub { 1; };
  my $node_cond = $cond->{node} || sub { 1; };
  if (my $in=$self->in) {
    my $parent = $in->source;
    return $parent if ($edge_cond->($in) && $node_cond->($parent));
  }
}

sub children {
  my $self = shift;
  my $cond = shift || {};		# undef or sub { }
  my $edge_cond = $cond->{edge};
  my $node_cond = $cond->{node};
  my @out = $self->all_out;
  if (ref($edge_cond) eq 'CODE') {
    @out = grep {$edge_cond->($_)} @out;
  }
  my @children = map {$_->target} @out;
  if (ref($node_cond) eq 'CODE') {
    @children = grep {$node_cond->($_)} @children;
  }
  return @children;
}

sub descendants {
  my $self = shift;
  my $cond = shift || {};		# undef or sub { }
  my $edge_cond = $cond->{edge};
  my $node_cond = $cond->{node};
  my $last_cond = $cond->{last};
  my @descendants = $self->children($last_cond->{cond});
  foreach my $middle ($self->children($cond)) {
##    next if (grep {$_ eq $middle} @descendants); # should not occur
    push(@descendants,$middle->descendants($cond));
  }
  return @descendants;
}

sub self_and_descendants {
  my $self = shift;
  my $cond = shift || {};		# undef or sub { }
  my $edge_cond = $cond->{edge};
  my $node_cond = $cond->{node};
  my $last_cond = $cond->{last} ||= {};
  my $last_node_cond = $last_cond->{node} ||= sub { 1; };
  my @descendants = ();
  push(@descendants,$self) if ($last_cond->{node}->($$self));
  push(@descendants,$self->descendants($cond));
  return @descendants;
}

sub first_ancestor {
  my $self = shift;
  my $cond = shift || {};
  my $edge_cond = $cond->{edge} ||= sub { 1; };
  my $node_cond = $cond->{node} ||= sub { 1; };
  my $first_cond = $cond->{first};
  my $first_edge_cond = $first_cond->{edge} ||= sub { 1; };
  my $first_node_cond = $first_cond->{node} ||= sub { 1; };
  if (my $in = $self->in) {
    my $parent = $in->source;
    if ($first_edge_cond->($in) && $first_node_cond->($parent)) {
      return $parent;
    } elsif ($edge_cond->($in) && $node_cond->($parent)) {
      $parent->first_ancestor($cond);
    }
  }
}

sub ancestors {
  my $self = shift;
  my $cond = shift || {};
  my $edge_cond = $cond->{edge} ||= sub { 1; };
  my $node_cond = $cond->{node} ||= sub { 1; };
  my $first_cond = $cond->{first};
  my $first_edge_cond = $first_cond->{edge} ||= sub { 1; };
  my $first_node_cond = $first_cond->{node} ||= sub { 1; };
  my @ancestors = ();
  if (my $in = $self->in) {
    my $parent = $in->source;
    if ($first_edge_cond->($in) && $first_node_cond->($parent)) {
      push(@ancestors,$parent);
    } 
    if ($edge_cond->($in) && $node_cond->($parent)) {
      push(@ancestors,$parent->ancestors($cond));
    }
  }
  return @ancestors;
}

foreach my $cat (qw/v nc np prep aux csu coo S N2 V CS det pro prel pri incise epsilon adj adv advneg VMod/) {
  no strict 'refs';
  *{__PACKAGE__."::is_$cat"} = sub {
    my $self = shift;
    my $lcat = $self->cat;
    return $lcat && ($cat eq $lcat);
  }
}

sub is_empty {
  my $self = shift;
  my $form = $self->form;
  return !$form;
}

sub get_subject {
  (shift->apply( dpath all_out 
		( is_subject target
		  union is_Infl is_adj target is_aux get_subject )))[0];
}

sub get_coord_subject {
  if (my $pred = (shift->apply( dpath in is_subst {$_->is_coord3 || $_->is_coord2} source is_coo in is_adj is_S source is_v ))[0]) {
    return $pred->really_try_get_subject;
  }
}

sub really_try_get_subject {
  my $self = shift;
  my $subject =  $self->get_subject
    || ($self->apply(dpath in is_subst is_xcomp source really_try_get_subject))[0]
    || $self->get_coord_subject
      || $self->get_participial_subject;
  if ($subject && $subject->is_prel) {
    ## try to find the antecedent
    my $newsubj = ($subject->apply(dpath in source is_v 
				   in is_subst is_SRel source
				   is_N2 in is_adj source
				  ))[0];
    return $newsubj || $subject;
  } else {
    return $subject
  }
}

sub is_neg {
  ## test whether a predicate is negative or not
  my $self = shift;
  if (my $neg = ($self->apply(dpath all_out is_adj is_Infl target is_aux is_neg))[0]) {
    return $neg;
  } elsif ($neg = ($self->apply(dpath all_out is_adj target is_advneg))[0]) {
    return $neg;
  }
  return undef;
}

sub arg_dative {
  shift->apply(dpath all_out is_preparg target {$_->lemma eq '�'}
	       all_out is_subst target);
}

sub arg_object {
  shift->apply(dpath all_out is_object target);
}

sub debug {
  my $self = shift;
  my $extra = "";
  if ($self->in) {
    $extra = " in=".$self->in->sprint;
  }
  $self->cas->log(2,"Node ".$self->sprint.$extra);
  return $self;
}

sub get_participial_subject {
  my $self = shift;
  ## need to take some case to avoid loops in rare cases
  ## should be fixed in easyforest in some future !
  ($self->apply( dpath debug
		 {$_->mode(qw/gerundive participle/)}
		 in is_subst {$_->is_S || $_->is_SubS}
		 source is_empty is_S
		 debug
		 in is_adj {$_->is_S || $_->is_vmod}
		 source is_v {$_ ne $self}
		 really_try_get_subject ))[0];
}

sub span {
  ## return the span of a node
  my $self = shift;
  my $cluster = $self->cas->id2obj($self->cluster);
  return [$cluster->att('left'),$cluster->att('right')];
}

sub xspan {
  ## return the span of the constituant headed by node
  my $self = shift;
  return $self->cas->deriv2span($self->deriv) || $self->span;
}

sub better_span {
  ## try to avoid some pb linked to epsilon nodes
  my $self = shift;
  my $span = $self->span;
  my $first = $self;
  my $left = $span->[0];
  my $last = $self;
  my $right = $span->[1];
##  $self->cas->log(5,"better span on [$left,$right]");
  foreach my $node ($self->descendants()) {
##    $self->cas->log(5,"better span node ".$node->sprint);
    my $form = $node->form;
    next if ($form =~ /^:[A-Z_ ]+$/ || $form =~ /^[A-Z_ ]+:/);
    my $span = $node->span;
    if (!$first || ($span->[0] < $left)) {
##      $self->cas->log(5,"left resize $left => $span->[0]");
      $left = $span->[0];
      $first = $node;
    }
    if (!$last || ($span->[1] > $right)) {
##      $self->cas->log(5,"right resize $right => $span->[1]");
      $right = $span->[1];
      $last = $node;
    }
  }
##  $self->cas->log(5,"better span => [$left,$right]");
  return [$left,$right];
}

sub left {
  return shift->span->[0];
}

sub right {
  return shift->span->[1];
}

sub xleft {
  return shift->xspan->[0];
}

sub xright {
  return shift->xspan->[1];
}

sub offset {
  ## return the offset of the node
  my $self = shift;
  $self->cas->span2pos($self->xml->parent,@{$self->span});
}

sub xoffset {
  ## return the offset of the const. headed by the node
  my $self = shift;
  $self->cas->span2pos($self->xml->parent,@{$self->xspan});
}

sub better_offset {
  ## return the offset of the const. headed by the node
  my $self = shift;
  $self->cas->span2pos($self->xml->parent,@{$self->better_span});
}

sub text {
  shift->offset->txt;
}

sub xtext {
  shift->xoffset->txt;
}

sub entity {
  my $self = shift;
  my $cluster = $self->cas->id2obj($self->cluster);
  if (my $occ = $cluster->att('#entity_occ')) {
    return $occ->parent;
  }
}

sub indirect_entity {
  ## entity is reached through a 'de' prep
  ( shift->apply(dpath all_out is_adj is_N2
		 target is_prep {$_->lemma eq 'de'}
		 all_out is_subst is_N2
		 target entity) ) [0];
}


sub precedes {
  my $self = shift;
  my $other = shift;
  my $span1 = $self->span;
  my $span2 = $other->span;
  return $span1->[1] < $span2->[0]
}

foreach my $x (qw{op top bot ht}) {
  has $x => ( is => 'ro',
	      isa => 'Maybe[XML::Twig::Elt]',
	      lazy_build => 1
	    );
}

sub _build_ht {
  my $self  = shift;
  my ($hid,$ht) = $self->cas->deriv2ht($self->deriv);
  return $ht;
}

sub _build_op {
  my $self  = shift;
  my ($oid,$op) = $self->cas->deriv2op($self->deriv);
  return $op;
}

sub _build_top {
  my $self  = shift;
  if (my $op = $self->op) {
    if (my $top = $op->first_child('narg[@type="top"]')) {
      return $top;
    }
  }
}

sub _build_bot {
  my $self  = shift;
  if (my $op = $self->op) {
    if (my $bot = $op->first_child('narg[@type="bot"]')) {
      return $bot;
    }
  }
}

sub _check_feature_value {
  my $xml  = shift;
  my $f = shift;
##  $xml->print(\*STDERR);
  my %values = (map {$_ => 1} @_);     
  return 0 unless ($xml);
  my $fs = $xml->first_child('fs');
  return 0 unless ($fs);
  my $fval = $fs->first_child("f[\@name='$f']");
  return 0 unless ($fval);
  foreach my $v ($fval->children) {
    my $type = $v->name;
    return 1 if (($type eq 'plus') && $values{'+'});
    return 1 if (($type eq 'minus') && $values{'-'});
    return 1 if (($type eq 'val') && $values{$v->trimmed_text});
  }
  return 0;
}

sub check_top_feature {
  my $self = shift;
  my $top = $self->top;
  return $top && _check_feature_value($top,@_);
}

sub check_bot_feature {
  my $self = shift;
  my $bot = $self->bot;
  return $bot && _check_feature_value($bot,@_);
}

sub check_ht_feature {
  my $self = shift;
  my $ht = $self->ht;
  return $ht && _check_feature_value($ht,@_);
}

foreach my $x (qw/diathesis refl/) {
  {
    no strict 'refs';
    *{__PACKAGE__."::$x"} = sub {
      shift->check_ht_feature($x,@_);
    };
  }
}

foreach my $x (qw/tense mode sat inv gender person extraction wh hum enum poss def det dem numberposs/) {
  {
    no strict 'refs';
    *{__PACKAGE__."::$x"} = sub {
      shift->check_top_feature($x,@_);
    };
    *{__PACKAGE__."::bot_$x"} = sub {
      shift->check_bot_feature($x,@_);
    };
  }
}

sub is_non_finite {
  shift->mode(qw/infinitive participle gerundive/);
}

sub is_finite {
  !shift->is_non_finite;
}

sub is_active {
  shift->diathesis('active');
}

sub is_passive {
  shift->diathesis('passive');
}

sub is_opening {
  my $self = shift;
  my $t = shift;
  $t =~ s/_/ /og;               # tmp solution for AUT_CL => AUT CL
  $self->form eq "${t}:";
}

sub is_closing {
  my $self = shift;
  my $t = shift;
  $t =~ s/_/ /og;		       # tmp solution for AUT_CL => AUT CL
  $self->form eq ":${t}";
}

sub is_within {
  my $self = shift;
  my $t = shift;
  ## $self->cas->log(4,"start is_within($t) ".$self->sprint);
  my $prev = $self;
  while ($prev) {
    ## $self->cas->log(4,"within left $t $prev");
    last if $prev->is_opening($t); 
    $prev = $prev->lprev;
  }
  return undef unless ($prev);
  my $next = $self;
  while ($next) {
    ## $self->cas->log(4,"within right $t $prev");
    last if $next->is_closing($t); 
    $next = $next->lnext;
  }
  return undef unless ($next);
  return ($prev,$next);
}

foreach my $x (qw/PRED SQ SHQ THQ HQ TQ AUT AUT_CL DQ/) {
  {
    no strict 'refs';
    my $open="is_opening_$x";
    my $close="is_closing_$x";
    my $in="is_in_$x";
    *{__PACKAGE__."::$open"} = sub {
      shift->is_opening($x);
    };
    *{__PACKAGE__."::$close"} = sub {
      shift->is_closing($x);
    };
    *{__PACKAGE__."::$in"} = sub {
      shift->is_within($x);
    };
  }
}

sub left_pred_satelite {
  my $self = shift;
  my $prev = $self->lprev;
  return unless($prev);
  ## $self->cas->log(4,"left satelite ".$self->sprint);
  return if ($prev->is_closing_PRED);
  foreach my $t (qw/AUT AUT_CL SHQ HQ/) {
    return ($t,$prev->lprev->is_within($t))
      if $prev->is_closing($t);
  }
  $prev->left_pred_satelite if ($prev);
}

sub right_pred_satelite {
  my $self = shift;
  my $next = $self->lnext;
  return unless ($next);
  ## $self->cas->log(4,"right satelite ".$self->sprint);
  return if ($next->is_opening_PRED);
  foreach my $t (qw/AUT AUT_CL TQ STQ/) {
    return ($t,$next->lnext->is_within($t))
      if $next->is_opening($t);
  }
  $next->right_pred_satelite if ($next);
}

sub is_full_parse {
  my $self = shift;
  $self->xml->parent->att('mode') eq 'full';
}

sub left_nearby_verbatim {
  my $self = shift;
  my $pos = $self->offset;
  my $where = $pos->where;
  my $start = $pos->start;
  my $xstart;
  my $xend;
  if (my $verbatims = $self->cas->{verbatims}{$where}) {
    foreach my $vstart (keys %{$verbatims}) {
      foreach my $vend (keys %{$verbatims->{$vstart}}) {
	if ($vend < $start && (($start-$vend) < 15)) {
	  if (!$xend || $xend < $vend) {
	    $xend = $vend;
	    $xstart = $vstart;
	  }
	}
      }
    }
    if ($xend) {
      return TeiProcess::Pos->new( where => $where, start => $xstart, end => $xend, cas => $self->cas);
    }
  }
}

sub right_nearby_verbatim {
  my $self = shift;
  my $pos = $self->offset;
  my $where = $pos->where;
  my $end = $pos->end;
  my $xstart;
  my $xend;
  if (my $verbatims = $self->cas->{verbatims}{$where}) {
    foreach my $vstart (keys %{$verbatims}) {
      foreach my $vend (keys %{$verbatims->{$vstart}}) {
	if ($end < $vstart && ($vstart-$end) < 8) {
	  if (!$xstart || $vstart < $xstart) {
	    $xend = $vend;
	    $xstart = $vstart;
	  }
	}
      }
    }
    if ($xstart) {
      return TeiProcess::Pos->new( where => $where, start => $xstart, end => $xend, cas => $self->cas);
    }
  }
}


no Moose;
##__PACKAGE__->meta->make_immutable;

1;
