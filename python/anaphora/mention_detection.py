#!/usr/bin/env python

'''
Class for MentionDetector: extraction of referential mentions from document

'''

import sys
from copy import deepcopy
from mention import Mention, Anaphor


'''
TODO:
    - citation: e.g. in/out citation; agent of citations 
    - better synchro with occurrences: more robust agr features (from different aliases)
    - non 3rd sg pronouns
    - non clitic pronouns

'''





class MentionDetector:

    def __init__(self, newssource):
        self.newssource = newssource
        self.mentions = []
        return


    def detect(self):
        self.mentions = []
        # detect mentions
        for sid in self.newssource.sentences:
            sentence = self.newssource.sentences[sid]
            parse = self.newssource.parses.get(sid)
            if parse:
                self.detect_nominals( parse )
                self.detect_anaphors( parse )
            else:
                print >> sys.stderr, "No parse for sentence %s" %sid
                # TODO: back-off detection...
                pass
        # sort mentions on position
        self.mentions.sort(key=lambda x:x.pos)
        # synchronize with entity/occurrences
        self.entity_sync()
        return


    def detect_nominals(self, parse):
        # find NPs headed by "nc" or "np"
        for nid in parse.nodes:
            node = parse.nodes[nid]
            if node.cat in ['nc','np']:
                np = Mention( node )
                self.mentions.append( np )
        return


    def detect_anaphors(self, parse):
        # find cln|cla|cld
        for nid in parse.nodes:
            node = parse.nodes[nid]
            if node.cat in ['cln','cla','cld']:
                # filter pleonastics
                if node.in_edge and node.in_edge.label == 'impsubj':
                    continue
                ana = Anaphor( node )
                # filter non 3rd pers sg (for now)
                if ana.person <> '3' or ana.number <> 'sg':
                    continue
                self.mentions.append( ana )
        return


    def entity_sync(self):
        occurrences = deepcopy(self.newssource.occurrences)
        print >> sys.stderr, len(self.mentions), "mentions"
        print >> sys.stderr, len(occurrences), "occurrences"
        for np in self.mentions:
            occ = self.newssource.occurrences.get(np.pos)
            # print >> sys.stderr, np, np.pos, occ
            if occ:
                eid = occ.get('entity_id')
                np.set_entity( self.newssource.entities[eid] )
                try:
                    occurrences.pop(np.pos)
                except KeyError: # pb: multiple NPs with same extent?? 
                    pass
        print >> sys.stderr, "Unmatched occurrences: %s/%s" %(len(occurrences),\
                                               len(self.newssource.occurrences))
        return




if __name__=="__main__":


    import re
    from news_reader import NewsSource
    import xml.etree.cElementTree as ET
    
    news_xml = sys.argv[1]
    xml_tree = ET.ElementTree()
    xml_tree.parse(news_xml)
    aggregate = xml_tree.getroot()
    creation_time = aggregate.get('creationTime')
    date = tuple(map(int, re.search(r'(\d+)\-(\d+)\-(\d+)', creation_time).groups()))
    if date and date > (2009,06,01):
        source = NewsSource(news_xml)
        detector = MentionDetector(source)
        detector.detect()
        for m in detector.mentions:
            print m

