'''
Class for Mention
'''

import re
import xml.etree.cElementTree as ET


third_pers_pro = re.compile(r"^(.*(ils?|elles?)|le|la|lui|l')$") # skip "on"
sg_pro = re.compile(r"^(j'|je|t'|tu|(-t-)?(il|elle)|le|la|lui|l')$")
# pl_pro = re.compile(r"^(nous|ils|elles|les)$")
masc_pro = re.compile(r"^(.*ils?|le)$")
fem_pro = re.compile(r"^(elles?|la)$")



class Mention(object):

    def __init__(self, node):
        self.node = node
        self.entity = None
        self.type = None 
        self.geoname = None
        self.xml_element = None
        self.set_lex()
        self.set_pos()
        self.set_morpho()
        return

    #### setters ####################################
    
    def set_lex(self):
        # lexical attributes
        self.form = self.node.form
        self.lemma = self.node.lemma
        self.text = self.node.get_text()
        return

    def set_pos(self):
        # positional attributes
        self.pos = self.node.get_pos()
        self.par_pos = self.pos[0]
        self.s_pos = self.pos[1]
        self.extent = self.pos[2]
        return
        
    def set_morpho(self):
        # morpho-syntactic attributes
        self.cat = self.node.cat
        agr_features = self.node.get_agr_features()
        self.gender = agr_features.get('gender')
        self.number = agr_features.get('number')
        self.person = agr_features.get('person')
        return

    def set_entity(self, entity):
        self.entity = True
        self.xml_element = entity.get('element')
        self.type = entity.get('type')
        self.geoname = entity.get('geoname')
        return
    
    # syntactic features
    # ...

    def __repr__(self):
        return "%s '%s' form: '%s' (P:%s, S:%s, OFF:%s): %s|%s|%s|%s|%s" \
               %(type(self).__name__,self.text,self.form,self.par_pos,self.s_pos,
                 self.extent, self.cat,self.gender,self.number,self.person,self.type)


    ##### accessors ####################################
    
    # distance features
    def sentDist( self, cand ):
        return abs(self.s_pos-cand.s_pos)

    def parDist( self, cand ):
        return abs(self.p_pos-cand.p_pos)

    #def npDist( self, cand ):
    #    return 

    #def wdDist( self, cand ):
    #    return

    #def hobbesDist( self, cand ):        
    #    return

    # morphological agreement/compatibility
    def number_agree( self, cand ):
        if self.number and cand.number:
            return self.number == cand.number 

    def gender_agree( self, cand ):
        if self.gender and cand.gender:
            return self.gender == cand.gender 

    def person_agree( self, cand ):
        if self.person and cand.person:
            return self.person == cand.person 

    def morph_agree( self, cand ):
        nagr = self.number_agree( self, cand )
        gagr = self.gender_agree( self, cand )
        pagr = self.person_agree( self, cand )
        return nagr and gagr and pagr 

    def number_compatible( self, cand ):
        if not (self.number and cand.number):
            return True
        return self.number == cand.number 

    def gender_compatible( self, cand ):
        if not (self.gender and cand.gender):
            return True
        return self.gender == cand.gender 

    def person_compatible( self, cand ):
        if not (self.person and cand.person):
            return True
        return self.person == cand.person 

    def morph_compatible( self, cand ):
        nagr = self.number_compatible( cand )
        gagr = self.gender_compatible( cand )
        pagr = self.person_compatible( cand )
        return nagr and gagr and pagr 

    # syntactic attributes
    # TODO
    # ...
    



class Anaphor(Mention):

    
    def set_morpho(self):
        # morpho-syntactic attributes
        self.cat = self.node.cat
        self.gender = None
        self.number = None
        self.person = None
        if third_pers_pro.match(self.form):
            self.person = "3"
        else:
            self.person = "^3"
        if sg_pro.match(self.form):
            self.number = 'sg'
        else:
            self.number = 'pl'
        if masc_pro.match(self.form):
            self.gender = 'masc'
        elif fem_pro.match(self.form):
            self.gender = 'fem'
        return


    def xml_linking(self, antecedent):
        if not antecedent.xml_element:
            return
        new_occ = ET.SubElement(antecedent.xml_element, "occ")
        new_occ.set("alias", self.form)
        new_occ.set("start", str(self.pos[2][0]))
        new_occ.set("end", str(self.pos[2][1]))
        new_occ.set("where", "parse_%s" %self.pos[0])
        token_ids = [t['id'] for t in self.node.get_tokens()]
        new_occ.set("tokens", " ".join(token_ids))
        new_occ.set("anaphora","1")
        return
    
