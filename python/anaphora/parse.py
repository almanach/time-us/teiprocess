#!/usr/bin/env python

'''
Class for Parse
'''

'''
TODO:
- tokens/extent accessor -> settor
- debug xextent and xspan!!!
- method for extracting GFs
- method for path from x to y
- position -> text
'''


import re
from collections import defaultdict
from node import Node
from edge import Edge



class Parse:

    def __init__(self, dep_elt):
        self.elt = dep_elt
        self.id = None 
        self.mode = None 
        self.nodes = {}
        self.edges = {}
        self.clusters = defaultdict(dict)
        self.derivs = defaultdict(dict)
        self._tokenspan2cluster = {} # id maps
        self._cluster2node = {}
        self._node2cluster = {}
        self._cluster2tokens = {}
        self.handle_dep()
        return

    ### handler methods
    def handle_dep(self):
        self.id = self.elt.get('id')
        self.mode = self.elt.get('mode')
        for sub_elt in self.elt.getchildren():
            if sub_elt.tag == 'cluster':
                self.handle_cluster(sub_elt)
            elif sub_elt.tag == 'node':
                self.handle_node(sub_elt)
            elif sub_elt.tag == 'edge':
                self.handle_edge(sub_elt)
            elif sub_elt.tag == 'op':
                self.handle_op(sub_elt)
            elif sub_elt.tag == 'hypertag':
                self.handle_hypertag(sub_elt)
        return


    def handle_cluster(self, elt):
        _id = elt.get('id')
        for k in elt.keys():
            self.clusters[_id][k] = elt.get(k)
        lex_ids = [l.split('|')[0] for l in elt.get('lex').split()]
        if lex_ids <> []:
            token_span = (lex_ids[0],lex_ids[-1])
            self._tokenspan2cluster[token_span] = _id
        self._cluster2tokens[_id] = lex_ids
        return

    
    def handle_node(self, elt):
        node = Node( elt, self )
        self.nodes[node.id] = node
        self._cluster2node[node.cluster] = node.id
        self._node2cluster[node.id] = node.cluster
        return
    

    def handle_edge(self, elt):
        edge = Edge( elt, self )
        self.edges[edge.id] = edge
        return



    def handle_op(self, elt):
        deriv = elt.get('deriv')
        self.derivs[deriv]['op'] = elt
        return


    def handle_hypertag(self, elt):
        deriv = elt.get('deriv')
        self.derivs[deriv]['ht'] = elt
        return

    
    ### accessor methods
    def get_edges(self):
        return edges


    def get_nodes(self):
        return nodes


    def find_node(self, token_span):
        cid = self._tokenspan2cluster.get(token_span)
        nid = self._cluster2node.get(cid) 
        return self.nodes.get(nid)



                              

    
    

    

if __name__=="__main__":

    import sys
    import xml.etree.cElementTree as ET
    from news_reader import NewsSource
    
    news_xml = sys.argv[1]
    news_source = NewsSource(news_xml)

    for pid in news_source.parses:
        parse = news_source.parses[pid]
        print ">> Parse %s: %s" %(parse.id, parse.mode)
        print ">> Nodes:", parse.nodes
        for node in parse.nodes.values():
            if node.is_main_subj():
                print "%s is main subj of sent %s" %(node,pid)
            elif node.is_main_obj():
                print "%s is main obj of sent %s" %(node,pid)
            elif node.is_main_obl():
                print "%s is main obl of sent %s" %(node,pid)
        print ">> Edges:", parse.edges
 
