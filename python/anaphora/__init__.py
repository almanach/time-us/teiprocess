"""
Anaphor resolver

"""

##//////////////////////////////////////////////////////
##  Metadata
##//////////////////////////////////////////////////////

# Version.  For each new release, the version number should be updated
# here and in the Epydoc comment (above).
__version__ = "0.0.1"

# Copyright notice
__copyright__ = """\
Copyright (C) 2009 Pascal Denis, all rights reserved
"""

__license__ = "Lesser GNU Public License"
# Description of the project, keywords, and the project's primary URL.
__longdescr__ = """Anaphora resolver"""
__keywords__ = []
__url__ = "None"

# Maintainer, contributors, etc.
__maintainer__ = "Pascal Denis"
__maintainer_email__ = "pdenis@inria.fr"
__author__ = "Pascal Denis"


# __all__ = [
#     ]
