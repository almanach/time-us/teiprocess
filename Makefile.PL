use inc::Module::Install;
use lib 'lib';

# Define metadata
name           'TeiProcess';
all_from       'lib/TeiProcess.pm';

# Specific dependencies
requires  'File::Find' => '0';
requires  'AppConfig' => '0';
requires  'Encode' => '0';
requires  'HTTP::Server::Simple' => '0';
requires  'Net::Server' => '0';
requires  'IPC::Run' => '0';
requires  'Test::More' => '0';
requires  'XML::Twig' => '0';

#test_requires  'Test::More'  => '0.42';
#recommends     'Text::CSV_XS'=> '0.50';
#no_index       'directory'   => 'demos';
install_script 'bin/tei_client.pl';
install_script 'bin/tei_process.pl';


$ENV{PERL5LIB} = "./lib:$ENV{PERL5LIB}";
##pmc_support;

auto_manifest;

installdirs 'site';

WriteAll;
