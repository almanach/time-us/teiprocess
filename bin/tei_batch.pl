#!/usr/bin/perl

## Run tei_process in batch mode
## Applying on all *.xml files under current directories

use 5.006;
use strict;
use warnings;
use Carp;

use POSIX qw(strftime setsid floor);

use IPC::Run qw(start finish run pump);
use AppConfig qw/:argcount :expand/;
use IO::File;
use IO::All qw/io/;
use EV;

my $config = AppConfig->new( "host|h=s@" => {DEFAULT => []},
			     "options=s" => {DEFAULT => "-Fnocache=1"},
			     "results=f",
			     "verbose|v!"        => {DEFAULT => 1},
			     "stats|s!"          => {DEFAULT => 0},
			     "timeout=i"  => {DEFAULT => 240},
			     "keep!" => {DEFAULR => 0} # keep old files
			   );

my $conffile = 'news_batch.conf';

if (@ARGV && $ARGV[0] =~ m/^--config/) {
  shift @ARGV;
  $conffile = shift @ARGV;
  print STDERR "USING config file $conffile\n";
}

if (-r $conffile) {
  $config->file("$conffile")
    or die "can't open or process configuration file $conffile, stopped";
}

$config->args();

print "Starting Dispatcher\n";
MyDispatcher->new($config);

package MyDispatcher;
use Carp;
use EV;
use IO::Socket;
use IO::All qw/io/;
use POSIX qw(strftime);
use File::Basename;

sub DESTROY {
  my $self = shift;
  $self->verbose("Quitting");
}

sub new {
  my $this   = shift;
  my $class  = ref($this) || $this;
  my $config = shift;

  my @hosts = @{$config->host};

  my $self = { config      => $config,
	       files => [glob("*.{xml,newsml}"),glob("*/*.{xml,newsml}")],
	       hosts => {}
	     };

  bless $self, $class;

  { no strict 'refs';
    ## the following to affect the adapted version of verbose to the dispatcher !
    ## should then be faster
    ## moreover, it avoids some strange problems when terminating the program
    ## with $config being destroyed before the dispatcher
    my $code;
    if ($config->verbose) {
      $code = sub {
	my $self = shift;
	my $date = strftime "[%F %H:%M:%S]", localtime;
	print "$date @_\n";
	print VERBOSE "$date @_\n" if (defined $self->{log});
      };
    } else {
      $code = sub {};
    }
    *{'verbose'} = $code;
  }

  my $results   = $self->results;
  my $log       = "$results/dispatch.log";

  mkdir $results;

  ## Open log files
  if ($config->get('verbose')) {
    open(VERBOSE,">>$log") or die "can't open log file $log: $!, stopped";
    VERBOSE->autoflush(1);
    $self->verbose("Activate log in $log");
    $self->{log} = $log;
  }

  ## trap deaths in callbacks
  $EV::DIED = sub {
    my $error = $@;
    $self->verbose('Event DIED');
    if (ref($error) eq "ErrorConnection") {
      ## intercept connection errors and reconnect
      $error->{client}->restart_connection;
    } else {
      $self->verbose("DIED: could not handle '$error'");
      $error->{client}->restart_connection;
    }
  };

  $self->{watchers} = [];

 push @{$self->{watchers}},
      EV::signal('QUIT', 
                 sub { print("GOT SIGQUIT!\n");
##                       $self->close_all_hosts;
                       EV::unloop(EV::UNLOOP_ALL);
                       $self->exit;
                     });

    push @{$self->{watchers}},
      EV::signal('ILL', 
                 sub { print("GOT SIGILL!\n");
##                       $self->close_all_hosts;
                       EV::unloop(EV::UNLOOP_ALL);
                       $self->exit;
                     });
    
    push @{$self->{watchers}},
      EV::signal('ABRT', 
                 sub { print("GOT SIGABRT!\n");
##                       $self->close_all_hosts;
                       EV::unloop(EV::UNLOOP_ALL);
                       $self->exit;
                     });

    push @{$self->{watchers}},
      EV::signal('TERM', 
                 sub { print("GOT SIGTERM!\n");
                       $self->verbose("GOT SIGTERM!");
##                       $self->close_all_hosts;
                       EV::unloop(EV::UNLOOP_ALL);
                       $self->exit;
                     });

  ## run two instances
  foreach my $host (@hosts) {
    my $hostlabel = $host;
    if ($host =~ /(\S+)\@(\S+)/) {
      $host = $2;
      $hostlabel = $1;
    }
    if (exists $self->{hosts}{$hostlabel}{client}) {
      $self->verbose("host '$host' already registered");
      next;
    }
    $self->run_on_host($host,$hostlabel);
  }

  EV::loop;

}

sub is_empty {
  !@{shift->{files}};
}

sub next_file {
  my $self = shift;
  return shift @{$self->{files}};
}

sub run_on_host {
  my $self = shift;
  my $host = shift;
  my $hostlabel = shift;

  my $results   = $self->{config}->results;
  my $url = "http://${host}:9087/process";
  my $options = $self->{config}->options;
  my $timeout = $self->{config}->timeout;

  while (@{$self->{files}}) {
    my $file = shift @{$self->{files}};
    
    unless ($file && -r $file) {
      $self->verbose("non existing $file");
      next;
    }

    my $keep = $self->{config}->keep;
    my $destfile = "$results/$file";

    if ($keep && -f $destfile && -s $destfile) {
      $self->verbose("$file already processed");
      next;
    }

    my $cmd = "curl -m $timeout $options -Fnews=\@$file --create-dirs -o $destfile $url";

    ##  print $cmd;

    if (my $pid = fork) {
      push @{$self->{watchers}},
	EV::child($pid,0,
		  sub { 
		    my ($w,$revents) = @_;
		    $self->verbose("[$hostlabel] Done $file");
		    if (my $status = $w->rstatus) {
		      $self->verbose("[$hostlabel] *** Got pb for $file: $!");
		    }
		    $self->run_on_host($host,$hostlabel);
		  });
      return;
    } else {
      $self->verbose("[$hostlabel] Running $file");
      exec($cmd);
    }
  }

}

sub results {
    my $self = shift;
    return $self->{config}->results;
}


sub warning {
    my $self = shift;
    print STDERR @_,"\n";
}

sub exit {
    my $self=shift;
    CORE::exit;
}


1;

