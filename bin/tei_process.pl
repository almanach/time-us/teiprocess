#!/usr/bin/perl

use strict;
use warnings;
use TeiProcess::Server;

## use Getopt::Long;
## use Pod::Usage;

use FindBin;
use lib "$FindBin::Bin/../lib";

my $daemon = shift;

# start the server on port 9087
if ($daemon && $daemon =~ /-daemon/) {
  my $pid = TeiProcess::Server->new(9087)
    ->background(
		 user => 'clergeri',
		 maxclients => 2,
		 pid_file => "/tmp/tei_process.pid",
		 log_level => 4
		);
  print "Use 'kill $pid' to stop server.\n";
} else {
  TeiProcess::Server->new(9087)
      ->run(
	    user => 'clergeri',
	    maxclients => 2,
	    pid_file => "/tmp/tei_process.pid",
	    log_level => 4
	   );
}

1;

=head1 NAME

tei_process.pl - Start TeiProcess HTTP server

=head1 SYNOPSIS

=head1 DESCRIPTION

Start a TeiProcess HTTP server

=head1 AUTHORS

Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2009, 2010, 2012, INRIA.

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
