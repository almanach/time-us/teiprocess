#!perl -T

use Test::More tests => 1;

BEGIN {
	use_ok( 'NewsProcess' );
}

diag( "Testing NewsProcess $NewsProcess::VERSION, Perl $], $^X" );
